from django.db import models
from django.db.models.deletion import CASCADE, SET
from django.contrib.postgres.fields import ArrayField

class Provincia(models.Model):
    nombre = models.CharField(max_length=100, null=False, unique=True)

    class Meta:
        db_table = 'provincias_de_argentina'
        verbose_name_plural = 'Provincias'
    
    def __str__(self):
        return str(self.id)

class Departamento(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    id_provincia = models.ForeignKey(Provincia, on_delete=CASCADE, null=False)

    class Meta:
        db_table = 'departamentos_de_provincias'
        verbose_name_plural = 'Departamentos'
    
    def __str__(self):
        return str(self.id)


class Localidad(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    id_departamento = models.ForeignKey(Departamento, on_delete=CASCADE, null=False)
    
    class Meta:
        db_table = 'localidades_de_departamentos_provincia'
        verbose_name_plural = 'Localidades'
    
    def __str__(self):
        return str(self.id)