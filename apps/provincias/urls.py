from django.urls import path
from . import apiviews

urlpatterns = urlpatterns = [
    path('listar-provincias/', apiviews.ListadoProvincias.as_view()),
    path('listar-departamentos/<id_provincia>/', apiviews.ListadoDepartamentosPorProvincia.as_view()),
    path('listar-localidades/<id_departamento>/', apiviews.ListadoLocalidadesPorDepartamento.as_view()),
]