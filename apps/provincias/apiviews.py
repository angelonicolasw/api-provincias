import json
import os
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.views import csrf_exempt
from rest_framework.generics import DestroyAPIView, ListAPIView, CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework import generics, response, status
from rest_framework.response import Response
from django.http.response import HttpResponseRedirect


from rest_framework import generics

from .serializers import *
from rest_framework import filters
from django.core.serializers import serialize
from django.http.response import Http404, HttpResponse, JsonResponse
from .models import Localidad, Provincia, Departamento

class ListadoProvincias(generics.ListAPIView):
    queryset = Provincia.objects.all()
    serializer_class = ProvinciaSerializer

class ListadoDepartamentosPorProvincia(generics.ListAPIView):
    serializer_class = DepartamentoSerializer

    def get_queryset(self):
        
        provincia = self.kwargs['id_provincia']
        return Departamento.objects.filter(id_provincia__id=provincia)

class ListadoLocalidadesPorDepartamento(generics.ListAPIView):
    serializer_class = LocalidadSerializer

    def get_queryset(self):
        
        departamento = self.kwargs['id_departamento']
        return Localidad.objects.filter(id_departamento__id=departamento)


