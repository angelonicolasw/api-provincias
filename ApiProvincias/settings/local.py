from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Permitido acceder de cualquier lugar
ALLOWED_HOSTS = ['127.0.0.1', '.herokuapp.com']

# Posgre Local
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'api-provincias',
        'USER': 'postgres',
        'PASSWORD': '1234',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}