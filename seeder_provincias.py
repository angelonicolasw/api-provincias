import os
import django
import pandas as pd
from openpyxl import Workbook, load_workbook

excelprov = load_workbook(filename='Prov-Dep-Local.xlsx',read_only=True)

# for sheet in excelprov:
#     print("Provincia: ",sheet.title)
#     ws = excelprov[sheet.title]
#     for row in ws.iter_rows(min_row=2, min_col=3, max_col=3):
#         for cell in row:
#             if cell.value != None:
#                 print(cell.value)
#     print("--------------------------------------------------------")

# # for sheet in excelprov:
# #     print(sheet.title)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ApiProvincias.settings.local')
django.setup()

from apps.provincias.models import Provincia, Departamento, Localidad

# Funcion para crear todas las provincias de Argentina

def crear_provincias():
    print("Agregando provincias a la BD...")
    print("----")
    
    for sheet in excelprov:
        try:
            Provincia.objects.create(nombre=sheet.title)
        except Exception as e:
            print(str(e))
        print("Se agrego la provincia: ", sheet.title)
    
    print("----")
    print("\n-----\nCarga de Provincias finalizo sin errores...\n\n\n")

# Funcion para crear todos los departamentos de provincias de Argentina

def crear_departamentos():
    print("+++++++++++++++++++++++++++++++++++++++++++++++++")
    print("Agregando Departamentos por Provincia a la BD...")
    print("----")

    for sheet in excelprov:
        ws = excelprov[sheet.title]
        print(sheet.title)
        
        try:
            provincia = Provincia.objects.get(nombre=sheet.title)
        except Exception as e:
            print(str,(e))
        
        for row in ws.iter_rows(min_row=2, min_col=3, max_col=3, values_only=True):
            for cell in row:
                if cell != None:
                    try:
                        Departamento.objects.create(nombre = cell, id_provincia = provincia)
                    except Exception as e:
                        print(str(e))
    
    print("\n-----\nCarga de Departamentos por Provincias finalizo sin errores...\n\n\n")

# # Funcion para crear todas las localidades de Argentina

def crear_localidades():
    print("+++++++++++++++++++++++++++++++++++++++++++++++++")
    print("Agregando Localidades por Departamentos con su correspondiente Provincia a la BD...")
    print("----")

    for sheet in excelprov:
        ws = excelprov[sheet.title]

        try:
            provincia = Provincia.objects.get(nombre=sheet.title)
        except Exception as e:
            print(str,(e))

        aux = '?none'
        print (sheet.title)
        print ("#######")
        for row in ws.iter_rows(min_row=2, min_col=1, max_col=2, values_only=True):
            
            if (row[0] != None and row[1] != None):
                if aux != row[0]:
                    aux = row[0]
                    
                    try:
                        departamento = Departamento.objects.filter(id_provincia = provincia).get(nombre=aux)
                    except Exception as e:
                        print(str(e))


                try:
                    Localidad.objects.create(id_departamento = departamento, nombre = row[1])
                except Exception as e:
                    print(str(e))
                    print(row)

    print("\n----\nCarga de Localidades finalizo sin errores...\n\n\n")



if __name__ == "__main__":
    crear_provincias()
    crear_departamentos()
    crear_localidades()

excelprov.close()



